import 'package:flutter/material.dart';

enum APP_THEME {LIGHT, DARK}
void main() {
  runApp(ContactProfilePage());
}
class MyAppTheme {
  static ThemeData appThemeLight(){
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.indigo.shade500,
        )
    );
  }

  static ThemeData appThemeDark(){
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
          color: Colors.black,
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.indigo.shade500,
        )
    );
  }
}

var appbar = AppBar(
  leading: Icon(
    Icons.arrow_back,
    // color: Colors.black,
  ),
  actions: <Widget>[
    IconButton(
        icon: Icon(Icons.star_border),
        // color: Colors.black,
        onPressed:(){
          print("Contact is starred");
        })
  ],
);

var body = ListView(
  children: <Widget>[

    Column(
      children: <Widget>[
        Container(
          width: double.infinity,

          //Height constraint at Container widget level
          height: 250,

          child: Image.network(
            "https://cms.dmpcdn.com/ugcarticle/2022/09/09/26acc9b0-3048-11ed-b847-fdd55281e83a_original.jpeg",
            fit: BoxFit.cover,
          ),

        ),
        Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.all(8.0),
                  child : Text(
                    "Sakda Onprom",
                    style: TextStyle(fontSize: 30),)
              )

            ],
          ),
        ),
        Divider(
          color: Colors.grey,
        ),

        Container(
          margin: const EdgeInsets.only(top: 8, bottom: 8),
          child : Theme (
            data: ThemeData(
            iconTheme: IconThemeData (
              color: Colors.pink,

            ),
          ),
          child: profileActionItem(),
          )
        ),
        Divider(
          color: Colors.grey,
        ),
        mobilePhoneListTile(),
        Divider(
          color: Colors.grey,
        ),
        otherPhoneListTile(),
        Divider(
          color: Colors.grey,
        ),
        emailListTile(),
        Divider(
          color: Colors.grey,
        ),
        addressListTile(),

      ],
    )
  ],
);
class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,

      theme: currentTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
      : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: appbar,
        body: body,
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.threesixty),
          onPressed: (){
            setState(() {
              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }
}
Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.textsms,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

Widget mobilePhoneListTile(){
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("098-091-7040"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      onPressed: (){},
    ),
  );
}

Widget otherPhoneListTile(){
  return ListTile(
    leading: Text(""),
    title: Text("080-012-0835"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      onPressed: (){},
    ),
  );
}

Widget emailListTile(){
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("sakdaonprom@gmail.com"),
    subtitle: Text("work"),
    trailing: Text("")
    ,
  );
}

Widget addressListTile(){
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("14 bangsaen"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      onPressed: (){},
    ),
  );
}

Widget profileActionItem(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton(),
    ],
  );
}


